# -*- mode: ruby -*-
# vi: set ft=ruby :

# Specify Vagrant version, Vagrant API version, and Vagrant clone location
Vagrant.require_version ">= 1.6.0"
VAGRANTFILE_API_VERSION = "2"
ENV['VAGRANT_VMWARE_CLONE_DIRECTORY'] = '~/.vagrant'
ENV["VAGRANT_DETECTED_OS"] = ENV["VAGRANT_DETECTED_OS"].to_s + " cygwin"

# Require 'yaml' and 'fileutils' modules
require 'yaml'
require 'fileutils'

# Read YAML file with VM details (box, CPU, RAM, IP addresses)
# Be sure to edit servers.yml to provide correct IP addresses
servers = YAML.load_file('servers.yml')

# Create and configure the VMs
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  # Always use Vagrant's default insecure key
  config.ssh.insert_key = false

  if Vagrant.has_plugin?("vagrant-vbguest")
    config.vbguest.auto_update = false
    config.vbguest.no_install = true
  end

  if Vagrant.has_plugin?("landrush")
    config.landrush.enabled = true
    config.landrush.tld = ".dev.geodan.nl"
    config.landrush.guest_redirect_dns = false
  end

  # Iterate through entries in YAML file to create VMs
  servers["servers"].each do |type, nodes|
    nodes.each do |server|
      config.vm.define server["name"] do |srv|
        # Don't check for box updates
        srv.vm.box_check_update = false
        srv.vm.hostname = server["name"] + ".dev.geodan.nl"
        srv.vm.box = server["box"]
        srv.vm.boot_timeout = 60
        srv.ssh.host = "localhost"

        # Assign an additional static private network
        srv.vm.network "private_network", ip: server["priv_ip"], nic_type: "virtio"

        # Configure VMs with RAM and CPUs per settings in servers.yml
        srv.vm.provider :virtualbox do |vb|
          vb.memory = server["ram"]
          vb.cpus = server["vcpu"]
          vb.gui = false
          vb.name = server["name"]
          vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
          vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
          vb.customize ["guestproperty", "set", :id, "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 1000]
          vb.customize ["modifyvm", :id, '--audio', 'none']
          vb.customize ["modifyvm", :id, "--vram", "12"]
        end

        # Copy SSH keys into the VM
        srv.vm.provision "file", source: "vagrant_id_rsa", destination: ".ssh/id_rsa"
        srv.vm.provision "file", source: "vagrant_id_rsa.pub", destination: ".ssh/id_rsa.pub"
        srv.vm.provision "shell", inline: "cat .ssh/id_rsa.pub >> .ssh/authorized_keys"
        # synchronize hardware clock
        srv.vm.provision "shell", inline: "/sbin/hwclock --systohc"

        # Disable default synced folder
        srv.vm.synced_folder ".", "/vagrant", disabled: true

        if type == "controller"
          # Enable default synced folder
          srv.vm.synced_folder ".", "/vagrant", :mount_options => ["dmode=644,fmode=644"]

          # Copy SSH keys into the VM
          srv.vm.provision "shell", inline: "touch ~/.ssh/known_hosts", privileged: false
          servers["servers"].each do |type, nodes|
            nodes.each do |server|
              if type != "ansible"
                srv.vm.provision "shell", inline: "ssh-keyscan -t rsa #{server['name']} 2>&1 | sort -u - ~/.ssh/known_hosts >> ~/.ssh/tmp_hosts", privileged: false
              end
            end
          end
          srv.vm.provision "shell", inline: "mv ~/.ssh/tmp_hosts ~/.ssh/known_hosts", privileged: false
          srv.vm.provision "shell", inline: "chmod 600 ~/.ssh/*", privileged: false
        end
      end
    end
  end
end
