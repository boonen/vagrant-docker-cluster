Ansible Playbooks for deploying a complete Docker Cloud Lab
===========================================================

The *vagrant-docker-cluster* project is a complete, runnable set of scripts to setup a local
Docker Cloud Lab. It is intended to demonstrate the use of Docker, Consul, Registrator,
Consul Template, cloud networking etc. All is packed in a [Vagrant](http://www.vagrantup.com) setup, to make distribution easy.

## Prerequisites
In order to run the demo, you should have the following software installed:
* [VirtualBox](https://www.virtualbox.org) (version 4.3.x; note: version 5.x has [issues](https://www.virtualbox.org/ticket/16084) with Vagrant networking)
* [Vagrant](http://www.vagrantup.com) (version 1.8.7 or higher)
* 2GB of free RAM

## Setup
The lab can be started by cloning this Git repo and issuing the correct vagrant commands. After setting up the Vagrant boxes we use [Ansible](http://www.ansible.com) to configure the Docker cloud.
1. `git clone https://gitlab.com/boonen/vagrant-docker-cluster.git`
2. `vagrant up` (this will take at least 10 minutes, because ~700MB of boxes need to be downloaded)
3. `vagrant ssh ansible`
4. `sudo su`
5. `cd /vagrant`
6. `ansible-playbook setup_cluster.yml` (this will take a few minutes)

### Known issues with the installation
For some reason VirtualBox does not always setup the NAT network correctly. When this happens Vagrant will hang and respond with a time-out error message:

    ==> cluster-node-01: Booting VM...
    ==> cluster-node-01: Waiting for machine to boot. This may take a few minutes...
        cluster-node-01: SSH address: localhost:2203
        cluster-node-01: SSH username: vagrant
        cluster-node-01: SSH auth method: private key
    Timed out while waiting for the machine to boot. This means that
    Vagrant was unable to communicate with the guest machine within
    the configured ("config.vm.boot_timeout" value) time period.
    
    If you look above, you should be able to see the error(s) that
    Vagrant had when attempting to connect to the machine. These errors
    are usually good hints as to what may be wrong.
    
    If you're using a custom box, make sure that networking is properly
    working and you're able to connect to the machine. It is a common
    problem that networking isn't setup properly in these boxes.
    Verify that authentication configurations are also setup properly,
    as well.
    
    If the box appears to be booting properly, you may want to increase
    the timeout ("config.vm.boot_timeout") value.

To fix this problem:
1. go to the VirtualBox GUI
2. turn off the virtual machine that 'hangs'
3. go back to the command line and run `vagrant up` again
