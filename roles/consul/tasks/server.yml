---
# tasks file for consul server

- name: Gather facts on Consul binary
  shell: /opt/consul/consul version | grep -Po "\d\.\d.\d"
  register: result
  ignore_errors: True

- name: Check whether Consul must be installed on a node
  set_fact:
    install_consul: "{{ result|failed or result.stdout != consul_version }}"

- name: Add group
  group:
    name: consul
    state: present

- name: Add user
  user:
    name: consul
    group: consul
    state: present

- name: Download Consul
  become: no
  delegate_to: 127.0.0.1
  run_once: true
  get_url:
    url: https://releases.hashicorp.com/consul/{{ consul_version }}/consul_{{ consul_version }}_linux_amd64.zip
    checksum: sha256:{{ consul_checksum_linux_amd64 }}
    dest: /tmp
    mode: 0440
  notify:
    - Cleanup

- name: Unarchive Consul
  become: no
  delegate_to: 127.0.0.1
  run_once: true
  unarchive:
    src: /tmp/consul_{{ consul_version }}_linux_amd64.zip
    dest: /tmp
    creates: /tmp/consul
  notify:
    - Cleanup

- name: Create Consul directory
  file:
    name: "{{ consul_directory }}"
    state: directory

- name: Install Consul
  copy:
    src: /tmp/consul
    dest: "{{ consul_directory }}/consul"
    owner: consul
    group: consul
    mode: 0755
  when: install_consul

- name: Add Consul directory to path
  lineinfile:
    dest: /etc/environment
    state: present
    line: 'PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:{{ consul_directory }}"'

- name: Create configuration directories
  file:
    path: "{{ consul_configuration_directory }}"
    owner: consul
    group: consul
    state: directory
    recurse: true

- name: Create data directory
  file:
    path: "{{ consul_data_directory }}"
    owner: consul
    group: consul
    state: directory
    recurse: true

- name: Create configuration
  template:
    src: config/{{ consul_agent_type }}.json.j2
    dest: "{{ consul_configuration_directory }}/{{ consul_agent_type }}.json"

- name: Create systemd script
  template:
    src: systemd/consul.j2
    dest: /etc/systemd/system/consul.service
    mode: 0644
    owner: consul
    group: consul
  when: ansible_service_mgr == 'systemd'
  notify:
    - Reload systemd

- name: Register Consul service to start on boot
  service:
    name: consul
    enabled: yes
    state: restarted

- name: Flush handlers
  meta: flush_handlers
